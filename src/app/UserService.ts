import {Injectable, NgZone} from "@angular/core";
import * as _ from "lodash";
import {GoogleAuthService} from "ng-gapi/lib/GoogleAuthService";
import GoogleUser = gapi.auth2.GoogleUser;
import GoogleAuth = gapi.auth2.GoogleAuth;

@Injectable()
export class UserService {
    public static readonly SESSION_STORAGE_KEY: string = "accessToken";
    public static readonly SESSION_STORAGE_KEY1: string = "expiresat";

    private user: GoogleUser = undefined;

    constructor(private googleAuthService: GoogleAuthService,
                private ngZone: NgZone) {
    }

    public setUser(user: GoogleUser): void {
        this.user = user;
    }

    public getCurrentUser(): GoogleUser {
        return this.user;
    }

    public getToken(): string {
        let token: string = sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
        if (!token) {
            throw new Error("no token set , authentication required");
        }
        return sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
    }

    public signIn() {
        this.googleAuthService.getAuth().subscribe((auth) => {
            auth.signIn().then(res => this.signInSuccessHandler(res), err => this.signInErrorHandler(err));
        });
    }
    public refreshToken() {
        console.log('refreshing')
        return new Promise((resolve, reject) => {
            this.googleAuthService.getAuth().subscribe((auth) => {
                let currUser = auth.currentUser.get();
                currUser.reloadAuthResponse().then((resp) => {
                    resolve(resp.access_token);
                }, (err) => {
                    reject(err);
                });
            }, (err) => reject(err));
        })
    }
    //TODO: Rework
    public signOut(): void {
        this.googleAuthService.getAuth().subscribe((auth) => {
            try {
                auth.signOut();
            } catch (e) {
                console.error(e);
            }
            sessionStorage.removeItem(UserService.SESSION_STORAGE_KEY)
            sessionStorage.removeItem(UserService.SESSION_STORAGE_KEY1)
            sessionStorage.clear()

        });
    }

    public isUserSignedIn(): boolean {
        // console.log('tocheck'+sessionStorage.getItem(UserService.SESSION_STORAGE_KEY))
        return !_.isEmpty(sessionStorage.getItem(UserService.SESSION_STORAGE_KEY));
    }

    private signInSuccessHandler(res: GoogleUser) {
        this.ngZone.run(() => {
            this.user = res;
            // console.log("to check setitems : "+sessionStorage.setItem(UserService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token))
            sessionStorage.setItem(
                UserService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token,
            );
            sessionStorage.setItem(
                UserService.SESSION_STORAGE_KEY1, (res.getAuthResponse().expires_at).toString(),
            );


        });
    }

    private signInErrorHandler(err) {
        console.warn(err);
    }
}