import { Component, OnInit } from '@angular/core';
import { GoogleAuthService } from 'ng-gapi';
import { UserService } from './UserService';
import { GoogleApiService } from 'ng-gapi';
import { SheetResource } from './SheetResource';
import {ActivatedRoute} from '@angular/router';
import {DriveResource} from './DriveResource'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'googleapitest2';
  public sheetId: string;
  public sheet: any;
  public foundSheet: any;

  constructor(private userService: UserService,
    private sheetResource: SheetResource,
    private route: ActivatedRoute,
    private authService: GoogleAuthService,
    private gapiService: GoogleApiService,
    private DriveResource:DriveResource
) {
// First make sure gapi is loaded can be in AppInitilizer
    this.gapiService.onLoad().subscribe();
      
  }
  ngOnInit() {
    this.route.fragment.subscribe((fragment) => {
    console.log(fragment)
})
}
  public isLoggedIn(): boolean {
    return this.userService.isUserSignedIn();
  }

  public signIn() {
    this.userService.signIn();
  }
  public signout() {
    this.userService.signOut();
  }

  public accessspreadsheets(){
    if(this.isLoggedIn){
      this.sheetResource.create(sessionStorage.getItem('accessToken')).subscribe((data)=>{
        console.log(data)
      })

    }
  }
  public reload(){
    this.userService.refreshToken()
  }
    public getspreadsheet(){
      if(this.isLoggedIn){
        this.sheetResource.findById('1O_BSgGBzE5iOdsDQrGIly7Mafr9oSXH9enWREbUoP_w',sessionStorage.getItem('accessToken')).subscribe((data)=>{
          console.log(data)
        })
        // console.log( this.sheetResource.findById('1O_BSgGBzE5iOdsDQrGIly7Mafr9oSXH9enWREbUoP_w',sessionStorage.getItem('accessToken')))
      }
  }
  public getdrivefile(){
    if(this.isLoggedIn){
      this.DriveResource.findById('1ksyK3op12QlXmG9AmDvK0gZtQ8Tv-jKW',sessionStorage.getItem('accessToken')).subscribe((data)=>{
        console.log(data)
      })
      // console.log( this.sheetResource.findById('1O_BSgGBzE5iOdsDQrGIly7Mafr9oSXH9enWREbUoP_w',sessionStorage.getItem('accessToken')))
    }
}
  
}
