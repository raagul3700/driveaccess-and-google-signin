import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class DriveResource {
    private readonly API_URL: string = 'https://www.googleapis.com/drive/v2';

    constructor(private httpClient: HttpClient) {
    }

    // authtoken as parameter only for demo purpose , better use a UserService to get the token
    public findById(drivefileid: string, authtoken: string): Observable<any> {
        return this.httpClient.get(this.API_URL + '/files/' + drivefileid, {
          headers: new HttpHeaders({
                Authorization: `Bearer ${authtoken}`
            })
        });
    }

    public create(authtoken: string): Observable<any> {
        return this.httpClient.post(this.API_URL,{}, {
          headers: new HttpHeaders({
                Authorization: `Bearer ${authtoken}`,                
            })
        });
    }
}
